import '../styles/globals.css'
import type { AppProps, AppContext } from 'next/app'

// Authentication
import { SSRKeycloakProvider, SSRCookies, Cookies } from '@react-keycloak/ssr'
import { IncomingMessage } from 'http'
import cookie from 'cookie'

interface AppPropsWithCookies extends AppProps {
  cookies: unknown
}

const MyApp = ({ Component, pageProps, cookies } : AppPropsWithCookies) => {
  
  const keycloakCfg = {
    url: 'https://syncrasy-sso-staging.apps.env02.syncrasy.dev/auth/',
    realm: 'syncrasy',
    clientId: 'hasura',
  }

  const keycloakInitOptions = {
    onLoad: 'login-required',
  }

  return (
    <SSRKeycloakProvider
      keycloakConfig={keycloakCfg}
      persistor={SSRCookies(cookies)}
      initOptions={keycloakInitOptions}
    >
      <Component {...pageProps} />
    </SSRKeycloakProvider>
  )
}

const parseCookies = (req?: IncomingMessage) =>
  !req || !req.headers ? {} : cookie.parse(req?.headers.cookie || '')

MyApp.getStaticProps = async (context: AppContext) => {
  return{
    cookies: parseCookies(context?.ctx?.req),
  }
}

export default MyApp
