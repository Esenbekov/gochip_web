import React, { FC } from 'react'
import withAuth from '../global/auth/withAuth'
import type { KeycloakInstance } from 'keycloak-js'
import { useKeycloak } from '@react-keycloak/ssr'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { ApolloClient, InMemoryCache, gql } from '@apollo/client'

const Index: FC<any> = ({ pets }) => {
  const { keycloak, initialized } = useKeycloak<KeycloakInstance>()
  console.log(pets)

  return (
    <div>
        <Head>
          <div className={styles.container}>
            <title>Go-chip Prototype</title>
          </div>
         </Head>
        {/* <div>The user is {keycloak?.authenticated ? '' : 'NOT'} authenticated</div>
        {keycloak?.authenticated && (
            <button type="button" onClick={() => keycloak.logout()}>
              Logout
            </button>
         )} */}
         <main className={styles.main}>
           <h1 className={styles.title}>
             Welcome to Go-chip Prototype
           </h1>
           <p className={styles.description}>
             Here is our request results
           </p>
           <div className={styles.grid}>
            {pets.map((pet, _index) => {
              return (
                <div className={styles.card} key="_index">
                  <h3>{pet.pet_name}</h3>
                  <p>Pet type: {pet.pets_type.pet_type_name}</p>
                  <p>Owner: {pet.persons_pets[0].person.person_name}</p>
                </div>
              )
            })}
           </div>
         </main>

      </div>
  )
}

export default withAuth(Index)

export async function getStaticProps() {

  const client = new ApolloClient({
    uri: 'https://hasura-graphql-engine-dev.apps.env02.syncrasy.dev/v1/graphql',
    cache: new InMemoryCache()
  })

  const {data} = await client.query({
    query: gql `
      query MyQuery {
        pets {
          pet_name
          pets_type {
            pet_type_name
          }
          persons_pets {
            person {
              person_name
              persons_vets {
                vet {
                  vet_name
                }
              }
            }
          }
        }
      }
    `,
    context: {
      headers: {
        'x-hasura-admin-secret': 'admin'
      },
    }
  })

  console.log('data', data)

  return {
    props: {
      pets: data.pets
    }
  }
}